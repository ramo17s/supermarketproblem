﻿using System;

namespace MarketPlace.Problem
{
    class Program
    {
        static void Main(string[] args)
        {
            const double priceApple = 0.2, priceOrange = 0.5, priceWatermelon = 0.8;
            int apple, orange, watermelon;
            double total = 0;
            Console.WriteLine("Welcome to our supermarket ! \n");
            Console.WriteLine("Give the quantity of Apple please :");
            apple = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Give the quantity of Orange please :");
            orange = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Give the quantity of Watermelon please :");
            watermelon = Convert.ToInt32(Console.ReadLine());

            while (apple >= 2)
            {
                total = total +  priceApple;
                apple = apple - 2;
            }
            if (apple > 0)
                total = total + priceApple * apple;



            while (orange >= 3)
            {
                total = total + 2 * priceOrange;
                orange = orange - 3;
            }
            if (orange > 0)
                total = total + priceOrange * orange;

            total = total + watermelon * priceWatermelon;
          
            Console.WriteLine("Total price is : {0}", total);
            Console.ReadKey();

        }
    }
}
